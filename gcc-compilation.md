##Installation
Installation on Ubuntu/Debian OS. make command should be present.(check using a terminal)

##Steps

1 Go to your home folder ($HOME)

2 Clone the repository

3 Copy the source file gcc-6.4.0-orginal.tar.gz  to your $HOME folder.

4 Extract the source file 
        
        tar -xvf gcc-6.4.0-orginal.tar.gz

5 Create directory of your wish (need not be gcc6.4). This under your $HOME folder.
        
          mkdir gcc6.4
              
6 Goto the gcc source folder
        
          cd gcc-6.4.0

7 Download the prerequesites for installing the gcc-6.4.0. The script in ./contrib folder of gcc-6.4.0 (current folder)
        
          ./contrib/download_prerequisites

8 Extract all the downloaded source file archieves.
 
 9 Go to folder created in step 5

          cd $HOME/gcc64

10 Configure gcc. Which also checks all the required dependencies are satisfied.  prefix option to configure tells where gcc should be installed. You can choose any name with the suffix $HOME/. Only C and C++  will be installed.

          ../gcc-6.4.0/configure --prefix=$HOME/GCC-6.4.2 --enable-languages=c,c++

11  Compile gcc souce using make utility. if make utility with required version number not there, install it.

          time make
12   Time to keep laptop kept charged and the bash terminal open. You can  focus on other work on laptop or elsewhere. Have food if not had. This step  took 236 seconds (nearly four hours) on mylaptop. make can be invoked parallely using -j option so that it finishes faster.

13 The gcc will be installed in bin folder of prefix given as argument on step 10. So it will be installed in $HOME/GCC-6.4.2/bin. Try to compile a c/c++ file on your machine.
        
        $HOME/GCC-6.4.2/bin   input_file_on_your_machine.pp

                
#Notes
1 The files downloaded during step 7 on my laptop, and gcc source file are in the repo. But do the step 7, not copy from repo.

2 make runs serially. make has -j k where make run parallely on k computing cores of a multicore cpu.

3 steps 1 to 13 are done in a bash terminal.
        

